
****************************************************************************************
IMPORTANT: THIS CODE IS FOR THE PRIVATE USE OF THE AUTHORS. YOU ARE NOT ALLOWED TO USE,
COPY, OR (RE-)DISTRIBUTE IT. THE AUTHORS DON'T TAKE RESPONSABILITY OF THE LEGAL PROBLEMS
DERIVED FROM ITS USE.

COPYRIGHT Albert Clapés aclapes (at) gmail (dot) com
****************************************************************************************

#-----------------#
    YouTubeMonk
#-----------------#

The idea behind this implementation is to download YouTube videos given a certain query in a string format (passed as
the program argument) and keep a record of the downloaded videos.

For each video, we keep the following information:
- The video itself.
- A meta-data file per video.
- A history file keeping record of all the processed videos, discarded ones, and the count of videos per author.

However, THIS ACTIVITY MAY CAUSE having IPs banned and even legal issues in the long term. Therefore, I created a script
that does so but in a less greedy way, minimizing the risks of being discovered doing such task. For that, the following
human-like behaviors are implemented:
- Waiting for a certain (random) time after getting the query results.
- Waiting for a certain (random) time after having downloaded the entire video and before submiting the next query
search. Not downloading all the videos from a results page of a query, but a random number of them (following a beta
distribution). That is, we can be downloading videos 3 out of 20 videos in the first page of results, and 9 in the
second. This will require several passes for the same query.
- Sometimes the order of download picks is randomized (so instead of downloading the videos 1->4->6, we go 4->1->6)

This is an example of execution:

   $ ./ytmonk.py "q&a videos" --videos-dir-path="videos/" --metadata-dir-path="metadata/" --history-path="history.pkl" --locale=en

   (if no arguments are passed after the query "videos/", "metadata/", and "history.pkl" are set as default)
   Type "-h" for more detail.

