#!/usr/bin/env python

__author__ = 'aclapes'

import time
import cPickle
import random
from os.path import isfile
import sys

import bs4
import requests
import numpy as np
from pytube import YouTube, exceptions
import argparse
import urllib, urllib2
import socket
from os import remove
import itertools

import thread
import threading

INTERNAL_PARAMETERS = dict(
    num_results=1000,  # number of results YT returns per search
    results_per_page=20,  # number of results got from each YT query

    beta_dist_for_downloads=dict(a=2,b=5),  # (human mimicking) number of videos to download from each YT query. TODO: change appropriately
    beta_dist_for_selecting=dict(a=2,b=2),  # (human mimicking) number of seconds to pick up a video after query response. TODO: change interval of (min time, max time) appropriately
    beta_dist_for_watching=dict(a=2,b=80),  # (human mimicking) the fraction of the video the monk will spend "watching". TODO: change appropriately
    ord_download_prob=0.7,  # (human mimicking) download from YT query in order or unordered.  TODO: change appropriately

    download_timeout=300,  # if activated videos are downloaded 720p, otherwise in 360p (standard quality)
    hd_quality=True,  # if activated videos are downloaded 720p, otherwise in 360p (standard quality)
    max_videos_per_author=3,  # TODO: change depending on requirements
)


def process_query(query_text, external_parameters):
    """
    Processes the query text with external parameters.

    Parameters
    ----------
    query_text: a text query to search in YouTube

    Returns
    -------
    No returns.

    """

    # create/load a structure for tracking queries
    # --------------------------------------------
    try:
        with open(external_parameters['history_path'], 'rb') as f:
            history = cPickle.load(f)
    except IOError:
        history = dict(
            queries=dict(),
            authors=dict()
        )

    # check the status of the query
    # -----------------------------
    # update history of queries
    if query_text in history['queries'] and history['queries'][query_text][0]:  # query has been made and it is completed (check bit)
        print('QUERY: %s -> COMPLETED' % query_text)
        return
    elif not query_text in history['queries']:  # never made, list it in history and mark as uncompleted
        history['queries'][query_text] = (False, dict())

    # control the completeness of this query
    all_videos_processed = True  # assume it's completed after this round


    # build the URL for the search
    # ----------------------------
    search_base_url = 'https://www.youtube.com/results?' \
                      + 'search_query=' + urlify(query_text) \
                      + ('&search_sort=' + external_parameters['filter_search_sort'] if external_parameters['filter_search_sort'] != 'relevance' is not None else '') \
                      + ('&filters=hd' if INTERNAL_PARAMETERS['hd_quality'] else '') \
                      + ((','+ external_parameters['filter_upload_date']) if external_parameters['filter_upload_date'] != 'all' else '')

    print 'QUERY:', search_base_url

    # Iterate over the query pages
    # ----------------------------

    num_pages = INTERNAL_PARAMETERS['num_results']/INTERNAL_PARAMETERS['results_per_page']
    for i in range(1,num_pages+1):
        search_url = search_base_url + ('&page=' + str(i) if i > 1 else '')

        response = requests.get(search_url)
        time.sleep(get_random_in_beta(INTERNAL_PARAMETERS['beta_dist_for_selecting']['a'],
                                    INTERNAL_PARAMETERS['beta_dist_for_selecting']['b'],
                                    8))

        # parse the response with beautiful soup and get the youtube ids from videos
        soup = bs4.BeautifulSoup(response.text, 'lxml')
        page_results = [d.get('data-context-item-id') for d in soup.find_all('div') if d.has_attr('data-context-item-id')]

        # build a list of candidate (non processed) videos
        candidates = []
        for id in page_results:
            for query_text, query_results in history['queries'].iteritems():  # never processed (in any of the queries)
                if not id in query_results[1]:
                    candidates.append(id)

        if len(candidates) == 0:  # all videos of this query page already processed
            continue

        if external_parameters['force_download_all']:
            num_downloads = len(candidates)
        else:
            # select how many videos to download from this query page as a
            # random variable following a beta distribution with parameters (a,b)
            num_downloads = int(np.round(
                 len(page_results) * np.random.beta(INTERNAL_PARAMETERS['beta_dist_for_downloads']['a'], \
                                                    INTERNAL_PARAMETERS['beta_dist_for_downloads']['b'], 1) ))
            if num_downloads < len(candidates):  # videos skipped?
                all_videos_processed = False  # another query will be required

            # select a sample of "num_downloads" videos
        video_sample = random.sample(np.linspace(0,len(candidates)-1,len(candidates), dtype=np.int32), \
                                     num_downloads if num_downloads <= len(candidates) else len(candidates))

        for j in video_sample:
            video_id = candidates[j]
            video_url = 'https://www.youtube.com/watch?v=' + video_id

            # get pointer to video object
            video_md = None  # video meta-data
            discarded = age_restricted = False  # if any of these, video is not going to be downloaded
            try:
                yt = YouTube(video_url)
                video_md = yt.get_video_data()['args']

                # *** few restrictions to not to download a candidate video ***
                # -------------------------------------------------------------
                # (1) if there's locale parameter in caption tracks (lc) check it is not different from 'en' (english)
                # those videos without lc info will need to be checked manually
                locale = parse_locale_info(video_md)
                if locale is not None and locale is not external_parameters['locale']:
                    discarded = True
                # (2) control the num of videos per author (having a maximum is parametrized)
                author = video_md['author'].encode('utf-8')
                if author in history['authors'] and len(history['authors'][author]) >= INTERNAL_PARAMETERS['max_videos_per_author']:
                    discarded = True
                # -------------------------------------------------------------
            except (exceptions.CipherError, exceptions.PytubeError, urllib2.URLError):
                discarded = True
            except exceptions.AgeRestricted:
                age_restricted = True

            if discarded or age_restricted:
                # update history file containing meta-data
                history['queries'][query_text][1][video_id] = False
                with open(external_parameters['history_path'], 'wb') as f:
                    cPickle.dump(history, f)
                continue

            # *** Otherwise, download ***

            video_relevant_md = get_relevant_metadata(video_md)
            print('DOWNLOAD: url=%s, title="%s", page=%d/%d' % (video_url, get_relevant_metadata(video_md)['title'], i, num_pages))

            # timeout = threading.Timer(video_relevant_md['length_seconds'], thread.interrupt_main)
            # timeout = threading.Timer(INTERNAL_PARAMETERS['download_timeout'], thread.interrupt_main)

            try:
                # timeout.start()
                yt.set_filename(video_id)  # video_id as filename of downloaded video (not video title)
                # select quality eventually "watch" the video
                video = yt.get('mp4', '720p' if INTERNAL_PARAMETERS['hd_quality'] else '360p')

                download_start_time = time.time()
                video.download(external_parameters['videos_dir_path'], on_progress=print_progress, force_overwrite=True)
                download_elapsed_time = time.time() - download_start_time
            except OSError:
                sys.stderr.write('# Warning : Conflicting filename.')
                continue
            except exceptions.DoesNotExist:
                sys.stderr.write('# Warning : Video does not exist in requested quality.')
                continue
            except (socket.timeout, KeyboardInterrupt):
                sys.stderr.write('# Warning : download request not responding.\n')
                try:
                    remove(external_parameters['videos_dir_path'] + video_id + '.mp4')
                except:
                    pass
                continue
            # timeout.cancel()

            # save meta-data
            with open(external_parameters['metadata_dir_path'] + video_id + '.pkl', 'wb') as f:
                cPickle.dump(video_md, f)

            # update and save history
            history['queries'][query_text][1][video_id] = True
            history['authors'].setdefault(video_relevant_md['author'], []).append(video_id)  # count the num of videos per author
            with open(external_parameters['history_path'], 'wb') as f:
                cPickle.dump(history, f)

            # Wait the monk while watching
            if video_relevant_md['length_seconds'] - download_elapsed_time > 0:
                sys.stdout.write("Monk is still 'watching', wait .. ")
                sys.stdout.flush()
                time.sleep(get_random_in_beta(INTERNAL_PARAMETERS['beta_dist_for_watching']['a'], \
                                              INTERNAL_PARAMETERS['beta_dist_for_watching']['b'], \
                                              video_relevant_md['length_seconds'] - download_elapsed_time))
                sys.stdout.write("Om Mani Padme Hum.\n")
                sys.stdout.flush()

            time.sleep(get_random_in_beta(INTERNAL_PARAMETERS['beta_dist_for_selecting']['a'], \
                                          INTERNAL_PARAMETERS['beta_dist_for_selecting']['b'],\
                                          8))

    # history['queries'][query_text][0] = all_videos_processed


def process_first_videos(k, external_parameters):
    '''
    Parameters
    ----------
    k
    external_parameters

    Returns
    -------

    '''
    with open(external_parameters['history_path'], 'rb') as f:
        history = cPickle.load(f)

def get_random_in_beta(a, b, factor=1.0):
    '''
    Returns a random number following a beta distribution with parameters (a,b).

    Parameters
    ----------
    a, b: the parameters of the beta distribution
    factor: a factor that multiplies the random number provided by the beta function.

    Return
    ------
    The random number.
    '''

    return int(np.round(factor * np.random.beta(a, b, 1)))


def get_relevant_metadata(video_md):
    """
    From all the metadata in video_md, it returns a dictionary with the value of the most relevant fields.

    Parameters
    ----------
    video_md : a dictionary with ALL the metadata of a YouTube video

    Returns
    -------
    A dictionary of relevant metadata
    """
    return dict(
        title=video_md['title'].encode('utf-8'),
        author=video_md['author'].encode('utf-8'),
        length_seconds=int(video_md['length_seconds'].encode('utf-8')),
        keywords=video_md['keywords'].encode('utf-8'),
        avg_rating=float(video_md['avg_rating'].encode('utf-8')),
        locale=parse_locale_info(video_md),  # a little bit more tricky
        iurl=video_md['iurl'].encode('utf-8')  # url of image thumbnail (not from video)
    )

def parse_locale_info(video_md):
    """
    From all the metadata in video_md, it returns the locale info if any or None otherwise.

    Parameters
    ----------
    video_md : a dictionary with ALL the metadata of a YouTube video

    Returns
    -------
    The locale info
    """
    if 'caption_tracks' in video_md:
        caption_tracks = video_md['caption_tracks'].encode('utf-8').split('&')
        for caption in caption_tracks:
            caption_parts = caption.split('=')
            if caption_tracks[0] == 'lc':
                return caption_tracks[1]
    return None


def urlify(str):
    '''
    Encodes a string in URL format.
    Example:
        'string_of_characters_like_these:$#@=?%^Q^$'
        to
        'string_of_characters_like_these%3A%24%23%40%3D%3F%25%5EQ%5E%24'
    More info: http://stackoverflow.com/questions/5607551/python-urlencode-string (stack overflow)

    Parameters
    ----------
    A normal string.

    Returns
    -------
    The encoded url for this string.
    '''
    return urllib.quote_plus(str)


def print_progress(received, total, clock):
    if received < total:
        p = int(float(received)/total * 100)
        sys.stdout.write("\r%d%%" % p)
    else:
        sys.stdout.write("\r\n")
    sys.stdout.flush()


if __name__ == "__main__":
    # there are two ways of invoking this script.
    # 1) Downloading videos of a particular query
    #    ex: "q&a videos" "questions and answers" --locale=en --force-download-all --videos-dir-path="/home/aclapes/Data/YoutubeMonk/videos/" --metadata-dir-path="/home/aclapes/Data/YoutubeMonk/metadata/" --history-path="/home/aclapes/Data/YoutubeMonk/history.pkl"
    # 2) Downloading the first k videos on channels we already got videos from
    #    ex: --k-first-videos=2 --videos-dir-path="/home/aclapes/Data/YoutubeMonk/videos/" --metadata-dir-path="/home/aclapes/Data/YoutubeMonk/metadata/" --history-path="/home/aclapes/Data/YoutubeMonk/history.pkl"

    # parse the input arguments
    parser = argparse.ArgumentParser(description='Download YouTube videos given a query.')
    parser.add_argument('queries', metavar='Q', type=str, nargs='*',
                   help='the query text')
    parser.add_argument('-F', '--force-download-all', action='store_true', help='otherwise only a subset of videos of each page are donwloaded.')
    parser.add_argument('--locale', help='language information (ex: "en" for english or "de" for deutsch)')
    parser.add_argument('--filter-upload-date', help='hour,today,week,month,year.')
    parser.add_argument('--filter-search-sort', help='video_date_uploaded,video_view_count,video_avg_rating.')

    parser.add_argument('-k', '--k-first-videos', help='download first k videos on channels we already got videos from.')

    parser.add_argument('--videos-dir-path', help='the directory where videos are downloaded.')
    parser.add_argument('--metadata-dir-path', help='the directory where metadata related to videos is kept (file per video).')
    parser.add_argument('--history-path', help='history (and meta-data) of the downloaded videos')


    args = parser.parse_args()

    external_parameters = dict()

    external_parameters['force_download_all'] = args.force_download_all

    filters = [
        ['day','week','month','year','all'],
        ['relevance','video_date_uploaded','video_view_count','video_avg_rating']
    ]
    if args.filter_upload_date:
        filters[0] = args.filter_upload_date
    if args.filter_search_sort:
        filters[1] = args.filter_search_sort

    filter_combinations = [filter for filter in itertools.product(*filters)]

    external_parameters['videos_dir_path'] ='videos/'
    if args.videos_dir_path:
        external_parameters['videos_dir_path'] = args.videos_dir_path

    external_parameters['metadata_dir_path'] ='metadata/'
    if args.metadata_dir_path:
        external_parameters['metadata_dir_path'] = args.metadata_dir_path

    external_parameters['history_path'] = 'history.pkl'
    if args.history_path:
        external_parameters['history_path'] = args.history_path

    external_parameters['locale'] ='en'  # english
    if args.locale:
        external_parameters['locale'] = args.locale

    # PROCEED downloading videos from the queries
    for query in args.queries:
        # random.seed(74)
        for comb in filter_combinations:  #random.sample(filter_combinations, len(filter_combinations)):
            external_parameters['filter_upload_date'] = comb[0]
            external_parameters['filter_search_sort'] = comb[1]
            process_query(query, external_parameters)

    if len(args.queries) == 0:
        process_first_videos(args.k_first_videos, external_parameters)